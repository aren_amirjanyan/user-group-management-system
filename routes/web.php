<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api','middleware' => 'auth'], function () use ($router) {
    /*Users*/
    $router->get('users',  ['uses' => 'UserController@getAllUsers']);

    $router->get('users/{id}', ['uses' => 'UserController@getOneUser']);

    $router->post('users', ['uses' => 'UserController@create']);

    $router->delete('users/{id}', ['uses' => 'UserController@delete']);

    $router->put('users/{id}', ['uses' => 'UserController@update']);

    /*Groups*/
    $router->get('groups',  ['uses' => 'GroupController@getAllGroups']);

    $router->get('groups/{id}', ['uses' => 'GroupController@getOneGroup']);

    $router->post('groups', ['uses' => 'GroupController@create']);

    $router->delete('groups/{id}', ['uses' => 'GroupController@delete']);

    $router->put('groups/{id}', ['uses' => 'GroupController@update']);

    /*Users Groups*/

    $router->post('users-groups', ['uses' => 'UserGroupController@create']);

    $router->delete('users-groups/{id}', ['uses' => 'UserGroupController@delete']);

    $router->put('users-groups/{id}', ['uses' => 'UserGroupController@update']);


});
