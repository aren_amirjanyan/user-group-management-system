<?php


namespace App\Contracts;

interface GroupServiceInterface
{
    public function getAllGroups();
    public function getGroupById($id);
    public function createGroup($data);
    public function updateGroup($id,$data);
    public function deleteGroup($id);

}