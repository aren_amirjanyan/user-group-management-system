<?php


namespace App\Contracts;

interface UserGroupServiceInterface
{
    public function createUserGroup($data);
    public function updateUserGroup($id,$data);
    public function deleteUserGroup($id);

}