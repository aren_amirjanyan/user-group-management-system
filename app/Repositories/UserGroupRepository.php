<?php
namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Exceptions\UserGroupNotFoundException;
use App\Models\UserGroup;

class UserGroupRepository extends BaseRepository {

    /**
     * @param $data
     * @return mixed
     */
    public function createUserGroup($data){

        return UserGroup::create($data);

    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws UserGroupNotFoundException
     */
    public function updateUserGroup($id,$data){

        $userGroup = UserGroup::find($id);

        if(!$userGroup)
            throw new UserGroupNotFoundException('User Group not found',404);

        $userGroup->update($data);

        return $userGroup;
    }

    /**
     * @param $id
     * @throws UserGroupNotFoundException
     */
    public function deleteUserGroup($id){

        $userGroup = UserGroup::find($id);

        if(!$userGroup)
            throw new UserGroupNotFoundException('User Group not found',404);

        $userGroup->delete();
    }
}