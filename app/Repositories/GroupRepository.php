<?php
namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Exceptions\GroupNotFoundException;
use App\Models\Group;

class GroupRepository extends BaseRepository {

    /**
     * @return Group[]|\Illuminate\Database\Eloquent\Collection
     * @throws GroupNotFoundException
     */
    public function getAllGroups(){
        $groups = Group::with('users.user')->get();
        if(!count($groups))
            throw new GroupNotFoundException('Groups not found!',404);
        return $groups;
    }

    /**
     * @param $id
     * @return mixed
     * @throws GroupNotFoundException
     */
    public function getGroupById($id){
        $group = Group::with('users.user')->where(['id' => $id])->get();
        if(!$group)
            throw new GroupNotFoundException('Group not found!',404);
        return $group;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createGroup($data){
        return Group::create($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws GroupNotFoundException
     */
    public function updateGroup($id,$data){
        $group = Group::find($id);

        if(!$group)
            throw new GroupNotFoundException('Group not found!',404);

        $group->update($data);

        return $group;
    }

    /**
     * @param $id
     * @throws GroupNotFoundException
     */
    public function deleteGroup($id){
        $group = Group::find($id);

        if(!$group)
            throw new GroupNotFoundException('Group not found!',404);

        $group->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function checkGroupById($id){
        return Group::find($id);
    }
}