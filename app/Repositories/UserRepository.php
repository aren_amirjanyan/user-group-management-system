<?php
namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Exceptions\UserNotFoundException;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository {

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     * @throws UserNotFoundException
     */
    public function getAllUsers(){
        $users = User::with('groups.group')->get();
        if(!count($users))
            throw new UserNotFoundException('Users not found',404);
        return $users;
    }

    /**
     * @param $id
     * @return mixed
     * @throws UserNotFoundException
     */
    public function getUserById($id){
        $user = User::with('groups.group')->where(['id' => $id])->get();
        if(!$user)
            throw new UserNotFoundException('User not found',404);
        return $user;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createUser($data){
        return User::create(array_merge($data,['password' => Hash::make($data['password']),'state' => User::NON_ACTIVE]));
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws UserNotFoundException
     */
    public function updateUser($id,$data){
        $user = User::find($id);

        if(!$user)
            throw new UserNotFoundException('User not found',404);

        (!empty($data['password'])) ? $user->update(array_merge($data,['password' => Hash::make($data['password'])])) : $user->update($data);

        return $user;
    }

    /**
     * @param $id
     * @throws UserNotFoundException
     */
    public function deleteUser($id){

        $user = User::find($id);

        if(!$user)
            throw new UserNotFoundException('User not found',404);

        $user->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function checkActiveUserById($id){
        return User::where(['id' => $id,'state' => User::ACTIVE])->first();
    }
}