<?php

namespace App\Services;

use App\Contracts\GroupServiceInterface;
use App\Core\Services\BaseService;
use App\Repositories\GroupRepository;

class GroupService extends BaseService implements GroupServiceInterface {
    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * GroupService constructor.
     * @param GroupRepository $groupRepository
     */
    public function __construct(
        GroupRepository $groupRepository
    )
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * @return \App\Models\Group[]|\Illuminate\Database\Eloquent\Collection
     * @throws \App\Exceptions\GroupNotFoundException
     */
    public function getAllGroups()
    {
        return $this->groupRepository->getAllGroups();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \App\Exceptions\GroupNotFoundException
     */
    public function getGroupById($id)
    {
        return $this->groupRepository->getGroupById($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createGroup($data)
    {
        return $this->groupRepository->createGroup($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\GroupNotFoundException
     */
    public function updateGroup($id,$data)
    {
        return $this->groupRepository->updateGroup($id,$data);
    }

    /**
     * @param $id
     * @throws \App\Exceptions\GroupNotFoundException
     */
    public function deleteGroup($id)
    {
        return $this->groupRepository->deleteGroup($id);
    }

}