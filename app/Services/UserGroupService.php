<?php

namespace App\Services;

use App\Contracts\UserGroupServiceInterface;
use App\Core\Services\BaseService;
use App\Exceptions\GroupNotFoundException;
use App\Exceptions\UserNotFoundException;
use App\Repositories\GroupRepository;
use App\Repositories\UserGroupRepository;
use App\Repositories\UserRepository;

class UserGroupService extends BaseService implements UserGroupServiceInterface {
    /**
     * @var UserGroupRepository
     */
    private $userGroupRepository;

    /**
     * UserGroupService constructor.
     * @param UserGroupRepository $userGroupRepository
     */
    public function __construct(
        UserGroupRepository $userGroupRepository
    )
    {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * @param $data
     * @return mixed
     * @throws GroupNotFoundException
     * @throws UserNotFoundException
     */
    public function createUserGroup($data)
    {
        if(!UserRepository::checkActiveUserById($data['user_id']))
            throw new UserNotFoundException('User Not found',404);

        if(!GroupRepository::checkGroupById($data['group_id']))
            throw new GroupNotFoundException('Group not found',404);

        return $this->userGroupRepository->createUserGroup($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws GroupNotFoundException
     * @throws UserNotFoundException
     * @throws \App\Exceptions\UserGroupNotFoundException
     */
    public function updateUserGroup($id, $data)
    {
        if(!UserRepository::checkActiveUserById($data['user_id']))
            throw new UserNotFoundException('User Not found',404);

        if(!GroupRepository::checkGroupById($data['group_id']))
            throw new GroupNotFoundException('Group not found',404);

        return $this->userGroupRepository->updateUserGroup($id,$data);
    }

    /**
     * @param $id
     * @throws \App\Exceptions\UserGroupNotFoundException
     */
    public function deleteUserGroup($id)
    {
        return $this->userGroupRepository->deleteUserGroup($id);
    }

}