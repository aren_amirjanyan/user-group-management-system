<?php

namespace App\Services;

use App\Contracts\UserServiceInterface;
use App\Core\Services\BaseService;
use App\Repositories\UserRepository;

class UserService extends BaseService implements UserServiceInterface {
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return \App\Models\User[]|\Illuminate\Database\Eloquent\Collection
     * @throws \App\Exceptions\UserNotFoundException
     */
    public function getAllUsers()
    {
        return $this->userRepository->getAllUsers();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \App\Exceptions\UserNotFoundException
     */
    public function getUserById($id)
    {
        return $this->userRepository->getUserById($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createUser($data)
    {
        return $this->userRepository->createUser($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\UserNotFoundException
     */
    public function updateUser($id,$data)
    {
        return $this->userRepository->updateUser($id,$data);
    }

    /**
     * @param $id
     * @throws \App\Exceptions\UserNotFoundException
     */
    public function deleteUser($id)
    {
        return $this->userRepository->deleteUser($id);
    }

}