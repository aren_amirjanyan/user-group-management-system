<?php

namespace App\Providers;

use App\Contracts\GroupServiceInterface;
use App\Services\GroupService;
use Illuminate\Support\ServiceProvider;

class GroupServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(GroupServiceInterface::class,GroupService::class);
    }
}
