<?php

namespace App\Providers;

use App\Contracts\UserGroupServiceInterface;
use App\Services\UserGroupService;
use Illuminate\Support\ServiceProvider;

class UserGroupServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserGroupServiceInterface::class,UserGroupService::class);
    }
}
