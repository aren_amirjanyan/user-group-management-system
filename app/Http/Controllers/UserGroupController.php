<?php

namespace App\Http\Controllers;

use App\Services\UserGroupService;
use Illuminate\Http\Request;

class UserGroupController extends Controller
{
    /**
     * @param Request $request
     * @param UserGroupService $userGroupService
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(
        Request $request,
        UserGroupService $userGroupService
    )
    {
        try {
            $this->validate($request, [
                'user_id'       => 'required',
                'group_id'      => 'required'
            ]);

            $data = $request->all();

            $user = $userGroupService->createUserGroup($data);
            $response = ['data' => $user,'success' => true, 'error' => false, 'message' => 'User Group successfully created!'];
            $status = 201;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    /**
     * @param $id
     * @param Request $request
     * @param UserGroupService $userGroupService
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(
        $id,
        Request $request,
        UserGroupService $userGroupService
    )
    {
        try {
            $this->validate($request, [
                'user_id'       => 'required',
                'group_id'      => 'required'
            ]);

            $data = $request->all();

            $user = $userGroupService->updateUserGroup($id,$data);
            $response = ['data' => $user,'success' => true, 'error' => false, 'message' => 'User Group successfully updated!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    /**
     * @param $id
     * @param UserGroupService $userGroupService
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(
        $id,
        UserGroupService $userGroupService
    )
    {
        try {
            $userGroupService->deleteUserGroup($id);
            $response = ['data' => [],'success' => true, 'error' => false, 'message' => 'User Group successfully deleted!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }
}