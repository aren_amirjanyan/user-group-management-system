<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUsers(
        UserService $userService
    )
    {
        try {
            $users = $userService->getAllUsers();
            $response = ['data' => $users,'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : $exception->getCode();
        }
        return response()->json($response,$status);
    }

    /**
     * @param $id
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneUser(
        $id,
        UserService $userService
    )
    {
        try {
            $user = $userService->getUserById($id);
            $response = ['data' => $user,'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : $exception->getCode();
        }
        return response()->json($response,$status);
    }

    /**
     * @param Request $request
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(
        Request $request,
        UserService $userService
    )
    {
        try {
            $this->validate($request, [
                'username'      => 'required|unique:users',
                'password'      => 'required',
                'first_name'    => 'required',
                'last_name'     => 'required',
                'email'         => 'required|email|unique:users'
            ]);

            $data = $request->all();

            $user = $userService->createUser($data);
            $response = ['data' => $user,'success' => true, 'error' => false, 'message' => 'User successfully created!'];
            $status = 201;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    /**
     * @param $id
     * @param Request $request
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(
        $id,
        Request $request,
        UserService $userService
    )
    {
        try {
            $this->validate($request, [
                'username'      => 'unique:users',
                'email'         => 'email|unique:users'
            ]);

            $data = $request->all();

            $user = $userService->updateUser($id,$data);
            $response = ['data' => $user,'success' => true, 'error' => false, 'message' => 'User successfully updated!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    /**
     * @param $id
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(
        $id,
        UserService $userService
    )
    {
        try {
            $userService->deleteUser($id);
            $response = ['data' => [],'success' => true, 'error' => false, 'message' => 'User successfully deleted!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }
}