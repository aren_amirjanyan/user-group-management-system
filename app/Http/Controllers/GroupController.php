<?php

namespace App\Http\Controllers;

use App\Services\GroupService;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * @param GroupService $groupService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllGroups(
        GroupService $groupService
    )
    {
        try {
            $groups = $groupService->getAllGroups();
            $response = ['data' => $groups,'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    /**
     * @param $id
     * @param GroupService $groupService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneGroup(
        $id,
        GroupService $groupService
    )
    {
        try {
            $group = $groupService->getGroupById($id);
            $response = ['data' => $group,'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    /**
     * @param Request $request
     * @param GroupService $groupService
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(
        Request $request,
        GroupService $groupService
    )
    {
        try {
            $this->validate($request, [
                'name'      => 'required|unique:groups'
            ]);

            $data = $request->all();

            $group = $groupService->createGroup($data);
            $response = ['data' => $group,'success' => true, 'error' => false, 'message' => 'Group successfully created!'];
            $status = 201;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    /**
     * @param $id
     * @param Request $request
     * @param GroupService $groupService
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(
        $id,
        Request $request,
        GroupService $groupService
    )
    {
        try {
            $this->validate($request, [
                'name'      => 'required|unique:groups'
            ]);

            $data = $request->all();

            $group = $groupService->updateGroup($id,$data);
            $response = ['data' => $group,'success' => true, 'error' => false, 'message' => 'Group successfully updated!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    /**
     * @param $id
     * @param GroupService $groupService
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(
        $id,
        GroupService $groupService
    )
    {
        try {
            $groupService->deleteGroup($id);
            $response = ['data' => [],'success' => true, 'error' => false, 'message' => 'Group successfully deleted!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }
}